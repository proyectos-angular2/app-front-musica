import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FormMusicalComponent } from './encuentas/components/form-musical/form-musical.component';
import { GraficosComponent } from './encuentas/components/graficos/graficos.component';

@NgModule({
  declarations: [
    AppComponent,
    FormMusicalComponent,
    GraficosComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
