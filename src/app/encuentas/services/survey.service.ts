import { Injectable } from '@angular/core';

import { Encuesta } from './encuesta';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, retry, throwError } from 'rxjs';


@Injectable({ providedIn: 'root' })

export class SurveyService {

  private serviceUrl: string = 'http://localhost:9061'
  private getEncuestas: string = '/api/v1/getListaEncuesta'
  private postEncuestas: string = '/api/v1/saveEncuesta'
  
  constructor( private http:HttpClient ) { }

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };
  

  // POST
  CreateSurvey(data: any): Observable<Encuesta> {
    return this.http
      .post<Encuesta>(
        this.serviceUrl + this.postEncuestas,
        JSON.stringify(data),
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.errorHandler));
  }

  // GET
  GetIssues(): Observable<Encuesta> {
    return this.http
      .get<Encuesta>(this.serviceUrl + this.getEncuestas)
      .pipe(retry(1), catchError(this.errorHandler));
  }

  errorHandler(error: { error: { message: string; }; status: any; message: any; }) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(() => {
      return errorMessage;
    });
  }
}
