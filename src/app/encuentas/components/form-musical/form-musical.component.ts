import { Component, NgZone, OnInit } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';
import { SurveyService } from '../../services/survey.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-musical',
  templateUrl: './form-musical.component.html',
  styleUrls: ['./form-musical.component.css']
})
export class FormMusicalComponent implements OnInit{

    ncstForm!: FormGroup;
    ncstArr: any = [];

    ngOnInit(): void {
       this.addNcstMusic(); 
    }

    constructor(
      public fb: FormBuilder,
      private ngZone: NgZone,
      private router: Router,
      public surveyService: SurveyService
    ) {}

    addNcstMusic() {
      this.ncstForm = this.fb.group({
        emailUsuario: [''],
        musicStyle: [''],
      })
    }

    submitForm() {
      this.surveyService.CreateSurvey(this.ncstForm.value).subscribe((res) => {
        console.log('Encuesta agregada!');
        this.ngZone.run(() => this.router.navigateByUrl('/graficos'))
      });
    }
}
